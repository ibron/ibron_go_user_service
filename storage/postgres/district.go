package postgres

import (
	"context"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"ibron/ibron_go_user_service.git/genproto/user_service"
	"ibron/ibron_go_user_service.git/pkg/helper"
	"ibron/ibron_go_user_service.git/storage"
)

type DistrictRepo struct {
	db *pgxpool.Pool
}

func NewDistrictRepo(db *pgxpool.Pool) storage.DistrictRepoI {
	return &DistrictRepo{
		db: db,
	}
}

func (c *DistrictRepo) Create(ctx context.Context, req *user_service.CreateDistrict) (resp *user_service.DistrictPrimaryKey, err error) {

	var district_id = uuid.New()

	query := `INSERT INTO "district" (
				district_id,
				title,
				region_id
			) VALUES ($1, $2, $3)
		`

	_, err = c.db.Exec(ctx,
		query,
		district_id.String(),
		req.GetTitle(),
		req.GetRegionId(),
	)

	if err != nil {
		return nil, err
	}

	return &user_service.DistrictPrimaryKey{DistrictId: district_id.String()}, nil
}

func (c *DistrictRepo) GetByPKey(ctx context.Context, req *user_service.DistrictPrimaryKey) (*user_service.District, error) {

	query := `
		SELECT
			district_id,
			title,
			region_id
		FROM "district"
		WHERE district_id = $1
	`
	var resp user_service.District

	err := c.db.QueryRow(ctx, query, req.GetDistrictId()).Scan(
		&resp.DistrictId,
		&resp.Title,
		&resp.RegionId,
	)

	if err != nil {
		return nil, err
	}

	return &resp, nil
}

func (c *DistrictRepo) GetAll(ctx context.Context, req *user_service.GetListDistrictRequest) (resp *user_service.GetListDistrictResponse, err error) {

	resp = &user_service.GetListDistrictResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			district_id,
			title,
			region_id
		FROM "district"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	if len(req.GetSearch()) > 0 {
		filter = " WHERE title ILIKE" + "'%" + req.Search + "%'"
	}

	query += filter + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	if err != nil {
		return resp, err
	}
	defer rows.Close()

	for rows.Next() {

		var district user_service.District

		err := rows.Scan(
			&resp.Count,
			&district.DistrictId,
			&district.Title,
			&district.RegionId,
		)

		if err != nil {
			return resp, err
		}

		resp.Districts = append(resp.Districts, &district)
	}

	return
}

func (c *DistrictRepo) Update(ctx context.Context, req *user_service.UpdateDistrict) (rowsAffected int64, err error) {

	query := `
			UPDATE
			    "district"
			SET
				title = $1,
				region_id = $2
			WHERE
				district_id = $3`

	result, err := c.db.Exec(ctx, query,
		req.GetTitle(),
		req.GetRegionId(),
		req.DistrictId,
	)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *DistrictRepo) Delete(ctx context.Context, req *user_service.DistrictPrimaryKey) error {

	query := `DELETE FROM "district" WHERE district_id = $1`

	_, err := c.db.Exec(ctx, query, req.GetDistrictId())

	if err != nil {
		return err
	}

	return nil
}
