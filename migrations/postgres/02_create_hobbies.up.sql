
CREATE TABLE IF NOT EXISTS "hobbies" (
    "id" UUID PRIMARY KEY,
    "name" VARCHAR(50) NOT NULL,
    "user_ids" VARCHAR(50)[],
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);

CREATE TABLE IF NOT EXISTS "user_hobbies" (
    "user_id" UUID  NOT NULL,
    "hobby_id" UUID NOT NULL,
    PRIMARY KEY ("user_id", "hobby_id"),
	FOREIGN KEY ("user_id") REFERENCES users ("id") ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY ("hobby_id") REFERENCES hobbies ("id") ON DELETE CASCADE ON UPDATE CASCADE
);
