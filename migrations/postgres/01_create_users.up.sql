
CREATE TABLE IF NOT EXISTS "region" (
    "region_id" uuid PRIMARY KEY NOT NULL,
    "title" VARCHAR NOT NULL
);

CREATE TABLE IF NOT EXISTS "district" (
    "district_id" uuid PRIMARY KEY NOT NULL,
    "title" VARCHAR NOT NULL,
    "region_id" uuid references region(region_id)
);

CREATE TABLE IF NOT EXISTS "users" (
    "id" uuid PRIMARY KEY,
    "first_name" VARCHAR NOT NULL,
    "last_name" VARCHAR NOT NULL,
    "phone_number" VARCHAR UNIQUE NOT NULL,
    "role" VARCHAR DEFAULT 'client',
    "username" VARCHAR DEFAULT '',
    "password" VARCHAR DEFAULT '',
    "status" INTEGER DEFAULT 1,
    "region_id" uuid NOT NULL references region(region_id),
    "district_id" uuid NOT NULL references district(district_id),
    "created_at" TIMESTAMP DEFAULT (now()),
    "updated_at" TIMESTAMP DEFAULT (now()),
    "deleted_at" TIMESTAMP
);
