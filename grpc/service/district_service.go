package service

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"ibron/ibron_go_user_service.git/config"
	"ibron/ibron_go_user_service.git/genproto/user_service"
	"ibron/ibron_go_user_service.git/grpc/client"
	"ibron/ibron_go_user_service.git/pkg/logger"
	"ibron/ibron_go_user_service.git/storage"
)

type DistrictService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*user_service.UnimplementedDistrictServiceServer
}

func NewDistrictService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *DistrictService {
	return &DistrictService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *DistrictService) Create(ctx context.Context, req *user_service.CreateDistrict) (resp *user_service.District, err error) {

	i.log.Info("---CreateDistrict------>", logger.Any("req", req))

	pKey, err := i.strg.District().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateDistrict->District->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.District().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyDistrict->District->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *DistrictService) GetById(ctx context.Context, req *user_service.DistrictPrimaryKey) (resp *user_service.District, err error) {

	i.log.Info("---GetDistrictByID------>", logger.Any("req", req))

	resp, err = i.strg.District().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetUserByID->District->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *DistrictService) GetList(ctx context.Context, req *user_service.GetListDistrictRequest) (resp *user_service.GetListDistrictResponse, err error) {

	i.log.Info("---GetDistricts------>", logger.Any("req", req))

	resp, err = i.strg.District().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetDistrict->District->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *DistrictService) Update(ctx context.Context, req *user_service.UpdateDistrict) (resp *user_service.District, err error) {

	i.log.Info("---UpdateDistrict------>", logger.Any("req", req))

	rowsAffected, err := i.strg.District().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateDistrict--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.District().GetByPKey(ctx, &user_service.DistrictPrimaryKey{DistrictId: req.DistrictId})
	if err != nil {
		i.log.Error("!!!GetDistrict->District->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *DistrictService) Delete(ctx context.Context, req *user_service.DistrictPrimaryKey) (resp *empty.Empty, err error) {

	i.log.Info("---DeleteDistrict------>", logger.Any("req", req))

	err = i.strg.District().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteDistrict->District->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
