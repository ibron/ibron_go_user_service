package postgres

import (
	"context"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"ibron/ibron_go_user_service.git/genproto/user_service"
	"ibron/ibron_go_user_service.git/pkg/helper"
	"ibron/ibron_go_user_service.git/storage"
)

type RegionRepo struct {
	db *pgxpool.Pool
}

func NewRegionRepo(db *pgxpool.Pool) storage.RegionRepoI {
	return &RegionRepo{
		db: db,
	}
}

func (c *RegionRepo) Create(ctx context.Context, req *user_service.CreateRegion) (resp *user_service.RegionPrimaryKey, err error) {

	var region_id = uuid.New()

	query := `INSERT INTO "region" (
				region_id,
				title
			) VALUES ($1, $2)
		`

	_, err = c.db.Exec(ctx,
		query,
		region_id.String(),
		req.GetTitle(),
	)

	if err != nil {
		return nil, err
	}

	return &user_service.RegionPrimaryKey{RegionId: region_id.String()}, nil
}

func (c *RegionRepo) GetByPKey(ctx context.Context, req *user_service.RegionPrimaryKey) (*user_service.Region, error) {

	query := `
		SELECT
			region_id,
			title
		FROM "region"
		WHERE region_id = $1
	`
	var resp user_service.Region

	err := c.db.QueryRow(ctx, query, req.RegionId).Scan(
		&resp.RegionId,
		&resp.Title,
	)

	if err != nil {
		return nil, err
	}

	return &resp, nil
}

func (c *RegionRepo) GetAll(ctx context.Context, req *user_service.GetListRegionRequest) (resp *user_service.GetListRegionResponse, err error) {

	resp = &user_service.GetListRegionResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			region_id,
			title
		FROM "region"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	if len(req.GetSearch()) > 0 {
		filter = " WHERE title ILIKE" + "'%" + req.Search + "%'"
	}

	query += filter + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	if err != nil {
		return resp, err
	}
	defer rows.Close()

	for rows.Next() {

		var region user_service.Region

		err := rows.Scan(
			&resp.Count,
			&region.RegionId,
			&region.Title,
		)

		if err != nil {
			return resp, err
		}

		resp.Regions = append(resp.Regions, &region)
	}

	return
}

func (c *RegionRepo) Update(ctx context.Context, req *user_service.UpdateRegion) (rowsAffected int64, err error) {

	query := `
			UPDATE
			    "region"
			SET
				title = $1
			WHERE
				region_id = $2`

	result, err := c.db.Exec(ctx, query,
		req.GetTitle(),
		req.GetRegionId(),
	)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *RegionRepo) Delete(ctx context.Context, req *user_service.RegionPrimaryKey) error {

	query := `DELETE FROM "region" WHERE region_id = $1`

	_, err := c.db.Exec(ctx, query, req.GetRegionId())

	if err != nil {
		return err
	}

	return nil
}
